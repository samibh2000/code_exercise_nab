﻿using Deposit.Service.Models;
using Deposit.Service.Persistance;
using Deposit.Service.Repository;
using Deposit.Service.Utility;
using Deposit.UI.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Deposit.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository _repository;
        /// <summary>
        /// Using DI
        /// </summary>
        /// <param name="repository"></param>
        public HomeController(IRepository repository)
        {
            _repository = repository;
        }

        public HomeController()
        {
            _repository = new DepositRepository();
        }

        public ActionResult Index()
        {
            return View(_repository.GetAll());
        }

        /// <summary>
        /// Called by the view to perform an add/delete operation
        /// The repository may not perform any operation if thresholds
        /// are not met (sum of aturity Amounts)
        /// </summary>
        /// <param name="OprId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateNeeded(UIEnums.Operations OprId)
        {
            bool changed = false;
            switch(OprId)
            {
                case UIEnums.Operations.Add:
                    changed = _repository.Add();
                    break;
                case UIEnums.Operations.Delete:
                    changed = _repository.Delete();
                    break;
            }

            return Json(new { data = changed }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// If update has been form reload the list of deposits.
        /// I have not added functionality for pagination to meet 
        /// the deadline
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.Client, Duration = 2)]
        public ActionResult PerformOpr()
        {
            return PartialView("_DepositList", _repository.GetAll());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}