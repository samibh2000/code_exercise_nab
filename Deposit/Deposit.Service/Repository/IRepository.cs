﻿using Deposit.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit.Service.Repository
{
    public interface IRepository
    {
        List<DepositModel> GetAll();
        bool Add();
        bool Delete();
    }
}
