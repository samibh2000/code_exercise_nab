﻿using Deposit.Service.Models;
using Deposit.Service.Persistance;
using Deposit.Service.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit.Service.Repository
{
    /// <summary>
    /// Mock repository
    /// </summary>
    public class DepositRepository : IRepository
    {
        private readonly IContext _context;

        /// <summary>
        /// Using DI
        /// </summary>
        /// <param name="context"></param>
        public DepositRepository(IContext context)
        {
            _context = context;
        }

        public DepositRepository()
        {
            _context = new MockContext();
        }

        /// <summary>
        /// Return all the deposits
        /// </summary>
        /// <returns></returns>
        public List<DepositModel> GetAll()
        {
            try
            {
                return _context.Deposits;
            }
            catch
            {
                // log the error;
                throw;
            }           
        }

        /// <summary>
        /// Add a new deposit if the sum of MaturityAmount is within the threshold
        /// </summary>
        /// <returns></returns>
        public bool Add()
        {
            try
            {
                if (GetAll().Sum(s => s.MaturityAmount) >= ConfigUtility.MaxTotalThreshold)
                {
                    return false;
                }

                Random random = new Random();

                DepositModel deposit = new DepositModel();
                deposit.Principal = random.Next(
                    ConfigUtility.MinPrincipal,
                    ConfigUtility.MaxPrincipal);
                deposit.Term = random.Next(1, 5);
                deposit.StartDate = DateTime.Now;
                deposit.EndDate = deposit.StartDate.AddYears(deposit.Term);
                deposit.InterestRate = random.Next(1, 5);
                deposit.MaturityAmount =
                    random.Next(ConfigUtility.MinMaturityAmount, ConfigUtility.MaxMaturityAmount);
                List<DepositModel> deposits = GetAll();
                deposits.Add(deposit);
                _context.Deposits = deposits;
                return true;
            }
            catch
            {
                // log the error;
                throw;
            }

        }

        /// <summary>
        /// Delete a deposit if the sum of MaturityAmount is within the threshold
        /// and principal meets the criteria with 3 to 5 millions
        /// </summary>
        /// <returns></returns>
        public bool Delete()
        {
            try
            {
                if (GetAll().Sum(s => s.MaturityAmount) <= ConfigUtility.MinTotalThreshold)
                {
                    return false;
                }
                DepositModel deposit = GetAll().
                    Where(q => q.Principal >= ConfigUtility.MinPrincipal &&
                    q.Principal <= ConfigUtility.MaxPrincipal).LastOrDefault();
                if (deposit != null)
                {
                    List<DepositModel> deposits = GetAll();
                    deposits.Remove(deposit);
                    _context.Deposits = deposits;
                    return true;
                }
                return false;
            }
            catch
            {
                // log the error;
                throw;
            }
        }
    }
}
