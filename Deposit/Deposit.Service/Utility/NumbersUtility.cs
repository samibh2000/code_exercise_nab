﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit.Service.Utility
{
    public class NumbersUtility
    {
        public static List<int> DistributeTotal(int minTotal, int maxTotal, int qty)
        {
            Random random = new Random();
            int sum = random.Next(minTotal, maxTotal);
            int max = sum / qty;

            int newNum = 0;
            List<int> genNumbers = new List<int>();
            for (int i = 0; i < qty - 1; i++)
            {
                newNum = random.Next(max);
                genNumbers.Add(newNum);
                sum -= newNum;
                max = sum / (qty - i - 1);
            }
            genNumbers.Add(sum);
            return genNumbers;
        }
    }
}
