﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit.Service.Utility
{
    public class ConfigUtility
    {
        public static int MinTotalRange
        {
            get { return 70000000; }
        }

        public static int MaxTotalRange
        {
            get { return 100000000; }
        }

        public static int NumberOfDeposits
        {
            get { return 50; }
        }

        public static int MinPrincipal
        {
            get { return 3000000; }
        }

        public static int MaxPrincipal
        {
            get { return 5000000; }
        }

        public static int MinMaturityAmount
        {
            get { return 5000000; }
        }

        public static int MaxMaturityAmount
        {
            get { return 7000000; }
        }

        public static int MinTotalThreshold
        {
            get { return 50000000; }
        }

        public static int MaxTotalThreshold
        {
            get { return 120000000; }
        }

    }
}
