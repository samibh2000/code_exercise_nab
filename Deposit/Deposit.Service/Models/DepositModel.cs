﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit.Service.Models
{
    public class DepositModel
    {
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Principal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate { get; set; }
        public decimal InterestRate { get; set; }
        public int Term { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal MaturityAmount { get; set; }
    }
}
