﻿using Deposit.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit.Service.Persistance
{
    public interface IContext
    {
        List<DepositModel> Deposits { get; set; }
    }
}
