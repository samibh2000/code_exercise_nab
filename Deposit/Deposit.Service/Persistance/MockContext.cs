﻿using Deposit.Service.Models;
using Deposit.Service.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deposit.Service.Persistance
{
    /// <summary>
    /// Mock context
    /// </summary>
    public class MockContext : IContext
    {
        private static List<DepositModel> _deposits;
        public List<DepositModel> Deposits
        {
            get
            {
                if (_deposits == null || _deposits.Count == 0)
                {
                    Seed(ConfigUtility.NumberOfDeposits);
                }
                return _deposits;
            }
            set
            {
                _deposits = value;
            }

        }

        /// <summary>
        /// Add first 50 deposits
        /// </summary>
        /// <param name="qty"></param>
        private void Seed(int qty)
        {
            List<int> maturityAmounts = NumbersUtility.DistributeTotal(
                ConfigUtility.MinTotalRange, 
                ConfigUtility.MaxTotalRange, 
                qty);
            _deposits = new List<DepositModel>();
            Random random = new Random();
            for (int counter = 0; counter < qty; counter++)
            {
                DepositModel deposit = new DepositModel();
                deposit.Principal = random.Next(
                    ConfigUtility.MinPrincipal - 1000000,
                    ConfigUtility.MaxPrincipal);
                deposit.Term = random.Next(1, 5);
                deposit.StartDate = DateTime.Now;
                deposit.EndDate = deposit.StartDate.AddYears(deposit.Term);
                deposit.InterestRate = random.Next(1, 5);
                deposit.MaturityAmount = maturityAmounts[counter];
                _deposits.Add(deposit);
            }
        }
    }
}
